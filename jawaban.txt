1.Membuat Database
CREATE DATABASE myshop;

2.Membuat Table di Dalam Database
CREATE TABLE users(
  id INT(5) AUTO_INCREMENT  PRIMARY KEY,
  `name` VARCHAR(255),
  email VARCHAR(255),
  `password` VARCHAR(255)
);

CREATE TABLE categories(
  id INT(5) AUTO_INCREMENT  PRIMARY KEY,
  `name` VARCHAR(255)
); 

CREATE TABLE items(
   id INT(5) AUTO_INCREMENT  PRIMARY KEY,
  `name` VARCHAR(255) NOT NULL,
   description VARCHAR(255) NOT NULL,
   price INT(12) NOT NULL,
   stock INT(5) NOT NULL,
   category_id INT(5),
   FOREIGN KEY (category_id) REFERENCES categories(id)
);


3. Memasukkan Data pada Table
INSERT INTO users (`name`,email,`password`)
VALUES ('John Doe','john@doe.com','john123'),
       ('Jane Doe','jane@doe.com','jenita123');


INSERT INTO categories (`name`)
VALUES ('gadget'),('cloth'),('men'),('women'),('branded');

INSERT INTO items (`name`,description,price,stock,category_id)
VALUES ('Sumsang b50','hape keren dari merek sumsang',4000000,100,1),
       ('Uniklooh','baju keren dari brand ternama',500000,50,2),
       ('IMHO Watch','jam tangan anak yang jujur banget',2000000,10,1);

4. Mengambil Data dari Database

a.
SELECT id,`name`,email FROM users;

b.
SELECT * FROM items WHERE price>1000000; 
SELECT * FROM items WHERE `name` LIKE '%uniklo%'; 

c.
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name
FROM items INNER JOIN categories ON items.category_id=categories.id;

5.Mengubah Data dari Database

UPDATE items SET price=2500000 WHERE `name`='Sumsang b50';

  